
#include "lib_search/search_interface.h"


int main() {

    struct top_files output;
    //инициализация массива вхождений нулями
    for(int i = 0 ; i < 5 ; ++ i )
    {
        output.intersections_in_files[i]=0;
        output.file_name[i]=NULL;
    }

    //вводим наименование директории с 'руки'
    char *directory;
    int length=0;
    scanf("%d",&length);
    directory=(char *)calloc(sizeof(char *),length);

    char *to_search;
    scanf("%d",&length);
    to_search=(char *)calloc(sizeof(char *),length);


    if(open_directory(directory,&output,to_search)==0)
    {
        return 1;
    }

    for(int i = 0 ; i < 5 ; ++ i )
    {
        printf("file is %s : counted elements : %d \n",output.file_name[i],output.intersections_in_files[i]);
        free(output.file_name[i]);
    }


    //аналогично , обращаюсь на статическую "test"
    //free(directory);
    return 0;
}