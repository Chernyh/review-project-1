//
// Created by znujko on 24.10.2019.
//

#ifndef SECOND_INDIVIDUAL_HOMETASK_SEARCH_WITH_PTHREAD_H
#define SECOND_INDIVIDUAL_HOMETASK_SEARCH_WITH_PTHREAD_H

#define likely(expr) __builtin_expect(!!(expr), 1)


#define unlikely(expr) __builtin_expect(!!(expr), 0)




#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <unistd.h>



struct searched_files
{

    int intersections_in_files;
    char *file_name;
    char *to_search;
};

struct top_files
{
    // всего 5 элементов , так как было сказано , что нужно находить топ-5 файлов !
    int intersections_in_files[5];
    char *file_name[5];
};


int file_way(char **way_to_file,const char *directory, const char *d_name);

int count_coincidences(const char *seek , FILE* file);

int adding_values_to_structer(const int counted,const char *filename,struct top_files *output);

void* searching_files_with_pthread(void *argument);

void initialize_pthreads(struct searched_files *output,pthread_t *thread_id);

int read_files_from_directory(const char *directory,struct dirent *de,DIR *dir,struct top_files *output,const char *to_search);

int open_directory(const char *directory,struct top_files *output,const char *to_search);



#endif //SECOND_INDIVIDUAL_HOMETASK_SEARCH_WITH_PTHREAD_H
