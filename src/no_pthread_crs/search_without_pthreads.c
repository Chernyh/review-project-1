#include "../../lib_search/search_interface.h"




int file_way(char **way_to_file,const char *directory, const char *d_name)
{
    *way_to_file=(char*)calloc(sizeof(char*),(strlen(directory)+strlen(d_name)+1));
    if(unlikely(*way_to_file==NULL))
    {
        printf("cannot calloc memory for %s",d_name);
        return 0;
    }
    strcpy(*way_to_file,directory);
    strcat(*way_to_file,"/");
    strcat(*way_to_file,d_name);
    return 1;
};

int count_coincidences(const char *seek , FILE* file)
{
    if(!file)
        return -1;
    if(seek==NULL)
        return -1;



    int items_counted = 0;
    char *readed=(char*)calloc(sizeof(char*),strlen(seek));
    while(fscanf(file,"%s",readed)!= EOF)
    {
        if(strncmp(readed,seek,strlen(readed))==0)
            ++items_counted;
    }
    free(readed);
    return items_counted;
};

int adding_values_to_structer(const int counted,const char *filename,struct top_files *output)
{
    if(output==NULL)
        return 0;
    if(output->intersections_in_files[4] < counted)
    {
        //магическая константа для того , чтобы пройти по всему массиву количества записей в топ-файлах и отсортировать их.
        int i = 4 ;
        while(i>=0 && output->intersections_in_files[i] < counted)
        {

            if(output->file_name[i-1]!=NULL && i!=0)
            {
                //занулем строку
                if(output->file_name[i]==NULL)
                {
                    output->file_name[i]=(char*)calloc(sizeof(char*),strlen(filename));
                    if(output->file_name[i]==NULL)
                    {
                        printf("cannot calloc memory for struct element!");
                        return 0;
                    }
                }
                output->file_name[i][0]='\0';
                strcpy(output->file_name[i],output->file_name[i-1]);
                output->intersections_in_files[i]=output->intersections_in_files[i-1];
            }
            --i;
        }
        ++i;

        if(output->file_name[i]==NULL)
        {
            output->file_name[i]=(char*)calloc(sizeof(char*),strlen(filename));
            if(output->file_name[i]==NULL)
            {
                printf("cannot calloc memory for struct element!");
                return 0;
            }
        }
        output->file_name[i][0]='\0';
        strcpy(output->file_name[i],filename);
        output->intersections_in_files[i]=counted;
    }
    return 1;
};

int read_files_from_directory(const char *directory,struct dirent *de,DIR *dir,struct top_files *output,const char *to_search)
{


    if(to_search==NULL)
    {
        printf("cannot initialize searchable element");
        return 0 ;
    }

    int counted = 0 ;
    while ((de = readdir(dir)) != NULL)
    {
       // printf("%s\n", de->d_name);
        if(likely(de->d_type == DT_REG))
        {
            char *way_to_file;
            if(unlikely(file_way(&way_to_file,directory,de->d_name)==0))
            {
                printf("cannot make way for file %s",de->d_name);
                return 0;
            }
            //printf("file way is : %s",way_to_file);
            FILE *file_in_dir = fopen(way_to_file,"r");
            if(likely(file_in_dir))
            {
                counted = count_coincidences(to_search,file_in_dir);
                // printf(" counted elements: %d \n",counted);
                if(adding_values_to_structer(counted,de->d_name,output)==0)
                {
                    printf("cannot add elemnts to structure");
                    return 0;
                }
                fclose(file_in_dir);
            }
            else
            {
                printf("cannot open :%s \n",de->d_name);
                return 0;
            }
            free(way_to_file);
        }
    }
    return 1;
};


// исходя из данных проверок read_files_from_directory выполняется
int open_directory(const char *directory,struct top_files *output,const char *to_search)
{
    if (unlikely(directory == NULL))  // opendir returns NULL if couldn't open directory
    {
        printf("Could not check directory" );
        return 0;
    }
    if (unlikely(output == NULL))  // opendir returns NULL if couldn't open directory
    {
        printf("Could not check struct of files" );
        return 0;
    }
    struct dirent de;

    if (unlikely(&de == NULL))  // opendir returns NULL if couldn't open directory
    {
        printf("Could not check struct dirent");
        return 0;
    }
    DIR *dir = opendir(directory);
    if (unlikely(dir == NULL))  // opendir returns NULL if couldn't open directory
    {
        printf("Could not open current directory" );
        return 0;
    }

    if(read_files_from_directory(directory,&de,dir,output,to_search)==0)
    {
        printf("cant read files from dir");
        return 0;
    }

    closedir(dir);
    return 1;
}
