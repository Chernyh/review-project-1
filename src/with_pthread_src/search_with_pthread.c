


#include "../../lib_search/search_interface.h"

#include <pthread.h>





int file_way(char **way_to_file,const char *directory, const char *d_name)
{
    *way_to_file=(char*)calloc(sizeof(char*),(strlen(directory)+strlen(d_name)+1));
    if(unlikely(*way_to_file==NULL))
    {
        printf("cannot calloc memory for %s",d_name);
        return 0;
    }
    strcpy(*way_to_file,directory);
    strcat(*way_to_file,"/");
    strcat(*way_to_file,d_name);
    return 1;
};

int count_coincidences(const char *seek , FILE* file)
{
    if(seek==NULL)
        return -1;
    if(!file)
        return -1;
    int items_counted = 0;
    char *readed=(char*)calloc(sizeof(char*),strlen(seek));
    while(fscanf(file,"%s",readed)!= EOF)
    {
        if(strncmp(readed,seek,strlen(readed))==0)
            ++items_counted;
    }
    free(readed);
    return items_counted;
};

int adding_values_to_structer(const int counted,const char *filename,struct top_files *output)
{
    if(output->intersections_in_files[4] < counted)
    {
        //магическая константа для того , чтобы пройти по всему массиву количества записей в топ-файлах и отсортировать их.
        int i = 4 ;
        while(i>=0 && output->intersections_in_files[i] < counted)
        {

            if(output->file_name[i-1]!=NULL && i!=0)
            {
                //занулем строку
                if(output->file_name[i]==NULL)
                {
                    output->file_name[i]=(char*)calloc(sizeof(char*),strlen(filename));
                    if(output->file_name[i]==NULL)
                    {
                        printf("cannot calloc memory for struct element!");
                        return 0;
                    }
                }
                output->file_name[i][0]='\0';
                strcpy(output->file_name[i],output->file_name[i-1]);
                output->intersections_in_files[i]=output->intersections_in_files[i-1];
            }
            --i;
        }
        ++i;

        if(output->file_name[i]==NULL)
        {
            output->file_name[i]=(char*)calloc(sizeof(char*),strlen(filename));
            if(output->file_name[i]==NULL)
            {
                printf("cannot calloc memory for struct element!");
                return 0;
            }
        }
        output->file_name[i][0]='\0';
        strcpy(output->file_name[i],filename);
        output->intersections_in_files[i]=counted;
    }
    return 1;
};





void* searching_files_with_pthread(void *argument)
{
    struct searched_files *file_that_is_checking=(struct searched_files *)argument;
    FILE *file_in_dir = fopen(file_that_is_checking->file_name,"r");
    if(likely(file_in_dir))
    {
        file_that_is_checking->intersections_in_files = count_coincidences(file_that_is_checking->to_search,file_in_dir);
        fclose(file_in_dir);
    }
    else
    {
        printf("cannot open :%s \n",file_that_is_checking->file_name);
        return 0;
    }

    pthread_exit(0);
};

void initialize_pthreads(struct searched_files *output,pthread_t *thread_id)
{
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_create(thread_id,&attr,searching_files_with_pthread,output);
};



int read_files_from_directory(const char *directory,struct dirent *de,DIR *dir,struct top_files *output,const char *to_search)
{
    int count_of_searchable_files=sysconf(_SC_NPROCESSORS_ONLN);
    if(unlikely(to_search==NULL))
    {
        printf("cannot initialize searchable element");
        return 0 ;
    }

    int counted = 0 ;
    struct searched_files file[count_of_searchable_files];
    pthread_t tids[count_of_searchable_files];
    while ((de = readdir(dir)) != NULL)
    {
        //printf("%s\n", de->d_name);
        if((de->d_type == DT_REG))
        {

            char *way_to_file;
            if(unlikely(file_way(&way_to_file,directory,de->d_name)==0))
            {
                printf("cannot make way for file %s",de->d_name);
                return 0;
            }
            file[counted].file_name=(char*)calloc(sizeof(char*),strlen(way_to_file));
            if(unlikely(file[counted].file_name==0))
            {
                printf("Error in itializing way to gile with pthreads!");
                return 0;
            }
            strcpy(file[counted].file_name,way_to_file);

            file[counted].to_search=(char*)calloc(sizeof(char*),strlen(to_search));
            if(unlikely(file[counted].to_search==0))
            {
                printf("Error in itializing searchable with pthreads!");
                return 0;
            }
            strcpy(file[counted].to_search,to_search);
            file[counted].intersections_in_files=0;
            initialize_pthreads(&file[counted],&tids[counted]);
            ++counted;

            free(way_to_file);
        }
        if(counted-1==count_of_searchable_files)
        {
            for(int i = 0 ; i < counted ; ++i)
            {
                pthread_join(tids[i],NULL);
                if(unlikely(adding_values_to_structer(file[i].intersections_in_files,file[i].file_name,output)==0))
                {
                    printf("cannot add elemnts to structure");
                    return 0;
                }
                free(file[i].file_name);
                free(file[i].to_search);
                file[i].intersections_in_files=0;
                //printf("filename is %s - counted : %d \n",file[i].file_name,file[i].intersections_in_files);
            }
            counted=0;
        }
    }


    for(int i = 0 ; i < counted ; ++i)
    {
        pthread_join(tids[i],NULL);
        if(unlikely(adding_values_to_structer(file[i].intersections_in_files,file[i].file_name,output)==0))
        {
            printf("cannot add elemnts to structure");
            return 0;
        }
        free(file[i].file_name);
        free(file[i].to_search);
        file[i].intersections_in_files=0;
        //printf("filename is %s - counted : %d \n",file[i].file_name,file[i].intersections_in_files);
    }
    return 1;
};

int open_directory(const char *directory,struct top_files *output,const char *to_search)
{
    if (unlikely(directory == NULL))  // opendir returns NULL if couldn't open directory
    {
        printf("Could not check directory" );
        return 0;
    }
    if (unlikely(output == NULL))  // opendir returns NULL if couldn't open directory
    {
        printf("Could not check struct of files" );
        return 0;
    }
    struct dirent de;
    if (unlikely(&de == NULL))  // opendir returns NULL if couldn't open directory
    {
        printf("Could not open current directory \n" );
        return 0;
    }
    DIR *dir = opendir(directory);
    if (unlikely(dir == NULL))  // opendir returns NULL if couldn't open directory
    {
        printf("Could not open current directory %s\n",directory );
        return 0;
    }

    if(read_files_from_directory(directory,&de,dir,output,to_search)==0)
    {
        printf("cant read files from dir \n");
        return 0;
    }
    closedir(dir);
    return 1;
};

