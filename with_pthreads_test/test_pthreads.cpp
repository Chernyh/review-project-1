//
// Created by znujko on 23.10.2019.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


extern "C" {
#include "../lib_search/search_interface.h"
}

#include "gtest/gtest.h"
#include <sys/timeb.h>
#include <fstream>
#include <string>
#include <iostream>
using namespace std;


////////////////////////////////////////////file way /////////////////////

TEST(does_initialize, computing_way_to_file)
{
    char* file;
    EXPECT_EQ(file_way(&file,"test","123"),1);
    free(file);
}


TEST(does_initialize, checking_true_way_to_file)
{
    char* file;
    file_way(&file,"test","12345");
    EXPECT_EQ((std::string)"test/12345",(std::string)file);
    free(file);
}


/////////////////////// count_concidences //////////////////////////



TEST(does_count, is_file)
{
    char* file;
    file_way(&file,"test","__345");
    FILE *file_is_not_in_dir=fopen(file,"r");
    EXPECT_EQ(count_coincidences("123",file_is_not_in_dir),-1);
    free(file);



}

TEST(does_count, char_free)
{
    char* file;
    file_way(&file,"test","1");
    FILE *file_is_in_dir=fopen(file,"r");
    EXPECT_EQ(count_coincidences(NULL,file_is_in_dir),-1);
    free(file);
    fclose(file_is_in_dir);
}

TEST(does_count, is_counting)
{
    char* file;
    file_way(&file,"test","1");
    FILE *file_is_in_dir=fopen(file,"r");
    EXPECT_NE(count_coincidences("125",file_is_in_dir),-1);
    free(file);
    fclose(file_is_in_dir);
}
///*
/////////////////////////////////////////open_dir//////////////////


TEST(does_initialize, cant_it_open_a_dir)
{
    struct top_files output;
    char *test1=(char *)"test1";
    char *t123=(char *)"123";
    EXPECT_EQ(open_directory(test1,&output,t123),0);
}


TEST(does_initialize, output_null)
{
    char *test1=(char *)"test1";
    char *t123=(char *)"123";
    struct top_files *output=NULL;
    EXPECT_EQ(open_directory(test1,output,t123),0);
}

TEST(does_initialize, dir_null)
{
    char *test1=(char *)"";
    char *t123=(char *)"123";
    struct top_files output;
    EXPECT_EQ(open_directory(test1,&output,t123),0);
}

////////////////////////////////////////////test_of_working/////////

TEST(work_test, test_of_working )
{
    struct top_files output;

    for(int i = 0 ; i < 5 ; ++ i )
    {
        output.intersections_in_files[i]=0;
        output.file_name[i]=NULL;
    }

    struct timeb t_start, t_current;
    int t_diff;
    ftime(&t_start);

    char *test=(char* )"../test";
    char *search=(char *)"125";
    open_directory(test,&output,search);
    ftime(&t_current);

    t_diff = (int) (1000.0 * (t_current.time - t_start.time)+ (t_current.millitm - t_start.millitm));
    /*
    std::string str="\nworking time of dynamic version is :";
    str+=std::to_string(t_diff);
    str+="ms";
    std::ofstream out("../README.md",ios::app);
    out << str;
    out.close();
     */
    printf("\n %d \n",t_diff);

    EXPECT_EQ(output.intersections_in_files[0],23);
    for(int i = 0 ; i < 5 ; ++ i )
        {
            output.intersections_in_files[i]=0;
            free(output.file_name[i]);
        }
    }
//*/